package Exercise1.data;

import Exercise1.ComicManage.Book;
import Exercise1.ComicManage.BookShelf;
import Exercise1.Contributor.Artist;
import Exercise1.Contributor.Author;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class DataReader {

    private static final String yes = "yes";
    private static final String no = "no";

    public void readToBook(String fileName, BookShelf bookShelf){
        try {
            Scanner scanner = new Scanner(new File(fileName));
            String buffer = new String();
            String token = new String();
            String s = "\n\t";
            StringTokenizer stringTokenizer;
            scanner.nextLine();
            while(scanner.hasNext()){
                Book book = new Book();
                buffer = scanner.nextLine();
                stringTokenizer = new StringTokenizer(buffer,s);
                token = stringTokenizer.nextToken();
                token.trim();
                book.setName(token);
                token = stringTokenizer.nextToken();
                token.trim();
                int a = Integer.parseInt(token);
                book.setPaper(a);
                token = stringTokenizer.nextToken();
                token.trim();
                if(token.equals(yes)){
                    book.setColor(true);
                }
                else book.setColor(false);

                token = stringTokenizer.nextToken();
                Author author = new Author();
                author.setName(token.trim());
                Author temp = bookShelf.existAuthor(author);
                if(temp == null)temp = author;
                book.setAuthor(temp);
                token = stringTokenizer.nextToken();
                Artist artist = new Artist();
                artist.setName(token);
                Artist tempArtist = bookShelf.existDrawArtist(artist);
                if(tempArtist==null)tempArtist = artist;
                book.setDrawArtist(tempArtist);
                token = stringTokenizer.nextToken();

                artist = new Artist();
                artist.setName(token);
                artist.setColor(true);
                tempArtist = bookShelf.existColorArtist(artist);
                if(tempArtist == null)tempArtist = artist;
                book.setColorArtist(tempArtist);
                bookShelf.addBook(book);
            }


        }catch (FileNotFoundException e){
            System.out.println(e);
        }
    }
}
