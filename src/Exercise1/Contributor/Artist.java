package Exercise1.Contributor;

public class Artist extends Author implements Comparable<Artist>{
    private boolean color = false; //if he is an colorful artist or not


    public boolean isColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }


    public int compareTo(Artist artist){
        return this.getName().compareTo(artist.getName());
    }

    public boolean equals(Artist artist){
        if(artist == null)return false;
        return this.getName().equals(artist.getName());
    }
}
