package Exercise1;

import Exercise1.ComicManage.BookShelf;
import Exercise1.data.DataReader;

public class Main {
    public  static void main(String []args){
        BookShelf bookShelf = new BookShelf();
        new DataReader().readToBook("src/Exercise1/data/books.txt", bookShelf);
        System.out.println(bookShelf.getBooks());
    }
}
