package Exercise1.ComicManage;

import Exercise1.Contributor.Artist;
import Exercise1.Contributor.Author;

import java.util.ArrayList;

public class BookShelf {
    private ArrayList<Book> books;

    public BookShelf(){
        books = new ArrayList<>();
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void addBook(Book book){
        books.add(book);
    }

    public Author existAuthor(Author author){
        for(Book book : books){
            if(book.getAuthor().equals(author)){
                return book.getAuthor();
            }
        }
        return null;
    }

    public Artist existDrawArtist(Artist artist){
        for(Book book : books) {
            if(book.getDrawArtist() == null)continue;
            if (book.getDrawArtist().equals(artist)) {
                return book.getDrawArtist();
            }
        }
        return  null;
    }

    public Artist existColorArtist(Artist artist){
        for(Book book: books){
            if(book.getColorArtist() == null)continue;
            if(book.getColorArtist().equals(artist)){
                return book.getDrawArtist();
            }
        }
        return null;
    }
}
