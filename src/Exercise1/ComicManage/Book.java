package Exercise1.ComicManage;

import Exercise1.Contributor.Artist;
import Exercise1.Contributor.Author;
import Exercise1.PaperControl.GlossyPaper;
import Exercise1.PaperControl.Paper;
import Exercise1.PaperControl.SandPaper;

public class Book {
    private static final int NORMAL_PAPER = 1;
    private static final int SAND_PAPER = 2;
    private static final int GLOSSY_PAPER = 3;

    private Artist colorArtist;
    private boolean color;
    private String name;
    private Paper paper;
    private Author author;
    private Artist drawArtist;

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(int paperCode) {
        if(paperCode == getNormalPaperCode()){
            this.paper = new Paper();
        }
        else if(paperCode == getSandPaperCode()){
            this.paper = new SandPaper();
        }
        else if(paperCode == getGlossyPaperCode()){
            this.paper = new GlossyPaper();
        }
    }

    public Artist getDrawArtist() {
        return drawArtist;
    }

    public void setDrawArtist(Artist drawArtist) {
        this.drawArtist = drawArtist;
    }

    public Artist getColorArtist() {
        return colorArtist;
    }

    public void setColorArtist(Artist colorArtist) {
        if(isColor())
            this.colorArtist = colorArtist;
    }


    public static int getNormalPaperCode() {
        return NORMAL_PAPER;
    }

    public static int getSandPaperCode() {
        return SAND_PAPER;
    }

    public static int getGlossyPaperCode() {
        return GLOSSY_PAPER;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }


    public boolean isColor() {
        return color;
    }

    public void setColor(boolean color) {
        if(paper == null){
            return;
        }
        if (paper.isColor())
            this.color = color;
        else this.color = false;
    }

    public String toString(){
        String a= new String();
        a += getName();
        a += (": " + getPaper().getName());
        a += "/";
        if(isColor())a+="colorful";
        else a+="black/white";
        a +="\n";
        a+= ("Author: " + getAuthor().getName() +"\n");
        a+= ("Draw artist: " + getDrawArtist().getName()+ "\n");
        if(isColor()) a+= ("Color artist: " + getColorArtist().getName() + "\n");

        return a;

    }
}
