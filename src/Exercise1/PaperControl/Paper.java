package Exercise1.PaperControl;

public class Paper {
    private boolean color = true;
    private double coef = 0;
    private String name = "normal paper";
    protected void setColor(boolean color){
        this.color = color;
    }

    public boolean isColor(){
        return color;
    }

    protected void setCoef(double coef){
        this.coef = coef;
    }

    protected void setName(String name){
        this.name = name;
    }

    public double getCoef(){
        return coef;
    }

    public String getName(){
        return name;
    }

}
