package Exercise1.PaperControl;

public class GlossyPaper extends Paper {
    public GlossyPaper(){
        setColor(true);
        setCoef(0.3);
        setName("glossy paper");
    }
}
