package Exercise2;

import java.util.Random;

public class Side implements Runnable{

    private int code;
    private Thread thread;
    private String name;
    private Rectangle rectangle;

    public Side(String name, Rectangle rectangle,int code){
        this.code =  code;
        this.rectangle = rectangle;
        this.name = name;
    }

    public void setSideValue(){

            int a;
            while(rectangle.status != code){
                try {
                    rectangle.wait();
                }catch (Exception e){
                    System.out.println(e);
                }
            }
            if(code == 1){
                rectangle.setSide1(a = new Random().nextInt(100));
                rectangle.status =2;
            }
            else{
                rectangle.setSide2(a = new Random().nextInt(100));
                rectangle.status = 3;
            }
            System.out.println(name + " : " + a);
            rectangle.notifyAll();
    }


    public void run(){
        while(true) {
            synchronized (rectangle) {

                try{
                    setSideValue();

                    Thread.sleep(1000);

                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }
    }

    public void start(){
        thread = new Thread(this, name);
        thread.start();
    }
}
