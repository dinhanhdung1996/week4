package Exercise2;


import java.util.concurrent.BlockingQueue;

public class Calculate implements Runnable{
    private  BlockingQueue queue;
    private Rectangle rectangle;
    private String name;
    private Thread thread;
    private int area;
    private int pirameter;

    public Calculate(Rectangle rectangle, String name) {
        this.queue = queue;
        this.rectangle = rectangle;
        this.name = name;
    }



    public void run(){

        while(true) {
            synchronized (rectangle) {
                while(rectangle.status!= 3){
                    try {
                        rectangle.wait();
                    }catch (Exception e){
                        System.out.println(e);
                    }
                }
                area = rectangle.getSide1() * rectangle.getSide2();
                pirameter = 2 * (rectangle.getSide2() + rectangle.getSide1());
                System.out.println("Width: " + rectangle.getSide1() + " Length: " + rectangle.getSide2()
                        + " Area: " + area + " Pirameter: " + pirameter);
                rectangle.status = 1;
                rectangle.notifyAll();
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }


    public void start(){
        thread = new Thread(this, name);
        thread.start();
    }


}
