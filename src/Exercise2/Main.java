package Exercise2;


public class Main {
    public static void main(String []args){
        Rectangle rectangle = new Rectangle();
        Side width = new Side("Width", rectangle, 1);
        Side length = new Side("Length", rectangle, 2);
        Calculate calculate = new Calculate(rectangle, "Rectangle");
        width.start();
        length.start();
        calculate.start();

    }
}
