package Exercise2;

public class Rectangle {
    int status = 1;
    private int side1;
    private int side2;

    public void setSide1(int side1){
        this.side1 = side1;

    }

    public void setSide2(int side2){
        this.side2 = side2;

    }

    public  int getSide1(){
        return side1;
    }

    public  int getSide2(){
        return side2;
    }
}
